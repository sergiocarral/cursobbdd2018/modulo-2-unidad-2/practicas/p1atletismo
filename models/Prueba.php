<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prueba".
 *
 * @property int $cod
 * @property int $codreu
 * @property int $numpru
 * @property int $codtip
 * @property string $horapru
 * @property string $lugarpru
 *
 * @property Reunion $codreu0
 * @property Tiposdeprueba $codtip0
 * @property Resultado[] $resultados
 */
class Prueba extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prueba';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codreu', 'numpru', 'codtip'], 'integer'],
            [['horapru'], 'safe'],
            [['lugarpru'], 'string', 'max' => 30],
            [['codreu', 'numpru'], 'unique', 'targetAttribute' => ['codreu', 'numpru']],
            [['codreu'], 'exist', 'skipOnError' => true, 'targetClass' => Reunion::className(), 'targetAttribute' => ['codreu' => 'codreu']],
            [['codtip'], 'exist', 'skipOnError' => true, 'targetClass' => Tiposdeprueba::className(), 'targetAttribute' => ['codtip' => 'codtip']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'codreu' => 'Codreu',
            'numpru' => 'Numpru',
            'codtip' => 'Codtip',
            'horapru' => 'Horapru',
            'lugarpru' => 'Lugarpru',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodreu0()
    {
        return $this->hasOne(Reunion::className(), ['codreu' => 'codreu']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodtip0()
    {
        return $this->hasOne(Tiposdeprueba::className(), ['codtip' => 'codtip']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultados()
    {
        return $this->hasMany(Resultado::className(), ['prueba' => 'cod']);
    }
}
