<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reunion".
 *
 * @property int $codreu
 * @property string $fechareu
 * @property string $lugarreu
 * @property string $nombrereu
 *
 * @property Prueba[] $pruebas
 */
class Reunion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reunion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechareu'], 'safe'],
            [['lugarreu', 'nombrereu'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codreu' => 'Codreu',
            'fechareu' => 'Fechareu',
            'lugarreu' => 'Lugarreu',
            'nombrereu' => 'Nombrereu',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPruebas()
    {
        return $this->hasMany(Prueba::className(), ['codreu' => 'codreu']);
    }
}
