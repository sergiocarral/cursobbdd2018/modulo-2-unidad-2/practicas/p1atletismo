<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deportista".
 *
 * @property int $coddep
 * @property string $nomapdep
 * @property string $provinciadep
 * @property string $fechanacimientodep
 * @property string $dnidep
 * @property string $domiciliodep
 * @property int $codposdep
 * @property string $telefonodep
 *
 * @property Resultado[] $resultados
 */
class Deportista extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deportista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechanacimientodep'], 'safe'],
            [['codposdep'], 'integer'],
            [['nomapdep', 'provinciadep', 'domiciliodep'], 'string', 'max' => 30],
            [['dnidep'], 'string', 'max' => 10],
            [['telefonodep'], 'string', 'max' => 14],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coddep' => 'Coddep',
            'nomapdep' => 'Nomapdep',
            'provinciadep' => 'Provinciadep',
            'fechanacimientodep' => 'Fechanacimientodep',
            'dnidep' => 'Dnidep',
            'domiciliodep' => 'Domiciliodep',
            'codposdep' => 'Codposdep',
            'telefonodep' => 'Telefonodep',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultados()
    {
        return $this->hasMany(Resultado::className(), ['coddep' => 'coddep']);
    }
}
