<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "resultado".
 *
 * @property int $cod
 * @property int $inscripcion
 * @property int $prueba
 * @property int $coddep
 * @property string $marcadep
 * @property string $posdep
 *
 * @property Deportista $coddep0
 * @property Prueba $prueba0
 */
class Resultado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resultado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inscripcion', 'prueba', 'coddep'], 'integer'],
            [['marcadep', 'posdep'], 'string', 'max' => 30],
            [['inscripcion', 'prueba'], 'unique', 'targetAttribute' => ['inscripcion', 'prueba']],
            [['coddep'], 'exist', 'skipOnError' => true, 'targetClass' => Deportista::className(), 'targetAttribute' => ['coddep' => 'coddep']],
            [['prueba'], 'exist', 'skipOnError' => true, 'targetClass' => Prueba::className(), 'targetAttribute' => ['prueba' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'inscripcion' => 'Inscripcion',
            'prueba' => 'Prueba',
            'coddep' => 'Coddep',
            'marcadep' => 'Marcadep',
            'posdep' => 'Posdep',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoddep0()
    {
        return $this->hasOne(Deportista::className(), ['coddep' => 'coddep']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrueba0()
    {
        return $this->hasOne(Prueba::className(), ['cod' => 'prueba']);
    }
}
