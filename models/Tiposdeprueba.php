<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tiposdeprueba".
 *
 * @property int $codtip
 * @property string $desttip
 *
 * @property Prueba[] $pruebas
 */
class Tiposdeprueba extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tiposdeprueba';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['desttip'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codtip' => 'Codtip',
            'desttip' => 'Desttip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPruebas()
    {
        return $this->hasMany(Prueba::className(), ['codtip' => 'codtip']);
    }
}
