<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Prueba */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prueba-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codreu')->textInput() ?>

    <?= $form->field($model, 'numpru')->textInput() ?>

    <?= $form->field($model, 'codtip')->textInput() ?>

    <?= $form->field($model, 'horapru')->textInput() ?>

    <?= $form->field($model, 'lugarpru')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
