<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Reunion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reunion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fechareu')->textInput() ?>

    <?= $form->field($model, 'lugarreu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombrereu')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
