<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tiposdeprueba */

$this->title = 'Update Tiposdeprueba: ' . $model->codtip;
$this->params['breadcrumbs'][] = ['label' => 'Tiposdepruebas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codtip, 'url' => ['view', 'id' => $model->codtip]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tiposdeprueba-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
