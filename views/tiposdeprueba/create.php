<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tiposdeprueba */

$this->title = 'Create Tiposdeprueba';
$this->params['breadcrumbs'][] = ['label' => 'Tiposdepruebas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiposdeprueba-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
