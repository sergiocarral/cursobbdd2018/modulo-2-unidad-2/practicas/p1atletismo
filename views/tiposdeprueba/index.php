<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tiposdepruebas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiposdeprueba-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tiposdeprueba', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codtip',
            'desttip',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
