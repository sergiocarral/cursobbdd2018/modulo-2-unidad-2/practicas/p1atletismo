<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Resultado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resultado-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'inscripcion')->textInput() ?>

    <?= $form->field($model, 'prueba')->textInput() ?>

    <?= $form->field($model, 'coddep')->textInput() ?>

    <?= $form->field($model, 'marcadep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'posdep')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
