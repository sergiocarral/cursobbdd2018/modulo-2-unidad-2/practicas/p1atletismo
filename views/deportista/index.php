<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deportistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deportista-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Deportista', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'coddep',
            'nomapdep',
            'provinciadep',
            'fechanacimientodep',
            'dnidep',
            //'domiciliodep',
            //'codposdep',
            //'telefonodep',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
