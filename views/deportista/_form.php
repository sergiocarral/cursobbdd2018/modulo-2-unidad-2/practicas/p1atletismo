<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Deportista */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deportista-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomapdep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'provinciadep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechanacimientodep')->textInput() ?>

    <?= $form->field($model, 'dnidep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'domiciliodep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codposdep')->textInput() ?>

    <?= $form->field($model, 'telefonodep')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
